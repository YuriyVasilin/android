package com.example.mylists.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mylists.model.Shop;

import java.util.ArrayList;


public class ShopListAdapter extends ArrayAdapter<Shop> {
    private Context context;
    private ArrayList<Shop> shops = new ArrayList<>();

    public ShopListAdapter(Context context, int textViewResourceId, ArrayList<Shop> shops) {
        super(context, textViewResourceId, shops);
        this.context = context;
        this.shops = shops;
    }
    @Override
    public int getCount() {
        return shops.size();
    }
    @Override
    public Shop getItem(int position) {
        return shops.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(shops.get(position).getName());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(shops.get(position).getName());

        return label;
    }
}
