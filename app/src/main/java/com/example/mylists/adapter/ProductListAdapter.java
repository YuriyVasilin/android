package com.example.mylists.adapter;

import com.example.mylists.activity.MainActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mylists.R;
import com.example.mylists.model.Product;

import java.util.ArrayList;

public class ProductListAdapter extends BaseAdapter {
    ArrayList<Product> mProducts = new ArrayList<>();
    Context mContext;
    LayoutInflater mInflater;

    public ProductListAdapter(ArrayList<Product> products, Context context) {
        mProducts = products;
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public int getCount() { return mProducts.size(); }

    @Override
    public Object getItem(int position){ return mProducts.get(position); }

    @Override
    public long getItemId(int position) {return position;}

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View view, ViewGroup parent){
        view = mInflater.inflate(R.layout.product_element, parent, false);
        if (mProducts.isEmpty()) return view;
        ((TextView) view.findViewById(R.id.tvElementProductName)).setText(mProducts.get(position).getProductName());
        ((TextView) view.findViewById(R.id.tvElementPrice)).setText(String.valueOf(mProducts.get(position).getPrice()));
        ((TextView) view.findViewById(R.id.tvElementAvail)).setText(String.valueOf(mProducts.get(position).getAvail()));
        if(position%2==1) ((LinearLayout) view.findViewById(R.id.llElement)).setBackgroundColor(
                mContext.getResources().getColor(R.color.odd_element)
        );

        LinearLayout sP = (LinearLayout) view.findViewById(R.id.tvAttributePrice);
        sP.setOnLongClickListener(
                spView -> {
                    MainActivity.sortByPrice();
                    return true;
                });
        TextView tP = (TextView) view.findViewById(R.id.textPrice);
        tP.setOnLongClickListener(
                tpView -> {
                    MainActivity.sortByPrice();
                    return true;
                });

        LinearLayout sA = (LinearLayout) view.findViewById(R.id.tvAttributeAvail);
        sA.setOnLongClickListener(
                saView -> {
                    MainActivity.sortByAvail();
                    return true;
                });
        TextView tA = (TextView) view.findViewById(R.id.textAvail);
        tA.setOnLongClickListener(
                taView -> {
                    MainActivity.sortByAvail();
                    return true;
                });

        return view;
    }

    public void colorChecked(int position, AdapterView<?> parent){
        View view;
        ListView listView = parent.findViewById(R.id.lvList2);
        for (int i = 0; i < mProducts.size(); ++i){
            view = parent.getChildAt(i);
            if (i % 2 == 1)
                ((LinearLayout) view.findViewById(R.id.llElement)).setBackgroundColor(
                        mContext.getResources().getColor(R.color.odd_element));
            else ((LinearLayout) view.findViewById(R.id.llElement)).setBackgroundColor(
                    mContext.getResources().getColor(R.color.white));

        }
        view = parent.getChildAt(position);
        if(listView.isSelected())
            ((LinearLayout) view.findViewById(R.id.llElement)).setBackgroundColor(
                    mContext.getResources().getColor(R.color.checked_element));
    }
}
