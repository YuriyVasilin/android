package com.example.mylists.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.mylists.R;
import com.example.mylists.activity.MainActivity;
import com.example.mylists.model.Category;
import com.example.mylists.model.Product;
import com.example.mylists.model.Shop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;



public class DB extends SQLiteOpenHelper {

    private SQLiteDatabase mdb;
    private int midShop;
    private int midCategory;
    private RequestQueue mrequestQueue;

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "products.db";
    private static final String CREATE_PRODUCT_TABLE = "create table if not exists " +
            Product.ProductContract.ProductEntry.TABLE_NAME + "(" +
            Product.ProductContract.ProductEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Product.ProductContract.ProductEntry.ProductName + " TEXT ," +
            Product.ProductContract.ProductEntry.ID_SHOP + " INTEGER NOT NULL " + "," +
            Product.ProductContract.ProductEntry.ID_CATEGORY + " INTEGER NOT NULL " + "," +
            Product.ProductContract.ProductEntry.INFO + " TEXT, " +
            Product.ProductContract.ProductEntry.AVAIL + " INTEGER, " +
            Product.ProductContract.ProductEntry.PRICE + " real " + ");";

    public DB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d("Database operations", "Database operates...");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PRODUCT_TABLE);
        Log.d("Database operations", "Tables created...");
    }
        /*public class ServerRequest extends AsyncTask <Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void voids) {
                return;
            }
        }*/

    public void addInfo(SQLiteDatabase db, Product product, RequestQueue requestQueue) {
        if(existProduct(db, product) && product.getIdShop()!=0) {
            updateProduct(db, product, requestQueue);
            Log.d("Database operations", "One row updated...");
        } else {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            ContentValues contentValues = new ContentValues();
            System.out.println("product from json addInfo " + product);
            contentValues.put(Product.ProductContract.ProductEntry.ProductName, product.getProductName());
            contentValues.put(Product.ProductContract.ProductEntry.ID_SHOP, product.getIdShop());
            contentValues.put(Product.ProductContract.ProductEntry.ID_CATEGORY, product.getIdCategory());
            contentValues.put(Product.ProductContract.ProductEntry.INFO, product.getInfo());
            contentValues.put(Product.ProductContract.ProductEntry.AVAIL, product.getAvail());
            contentValues.put(Product.ProductContract.ProductEntry.PRICE, product.getPrice());
            db.insert(Product.ProductContract.ProductEntry.TABLE_NAME, null, contentValues);
            Log.d("Database operations", "One row inserted...");
        }
    }
    public void deleteProduct(SQLiteDatabase db, Product product, RequestQueue requestQueue) {
        if(existProduct(db, product)) {
            db.delete(
                    Product.ProductContract.ProductEntry.TABLE_NAME,
                    Product.ProductContract.ProductEntry.ProductName +" = ? AND "+
                            Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND "+
                            Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?",
                    new String[] {String.valueOf(product.getProductName()),
                            String.valueOf(product.getIdShop()),
                            String.valueOf(product.getIdCategory())});
        }
    }
    public void updateProduct(SQLiteDatabase db, Product product, RequestQueue requestQueue) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Product.ProductContract.ProductEntry.ID_SHOP, product.getIdShop());
        contentValues.put(Product.ProductContract.ProductEntry.ProductName, product.getProductName());
        contentValues.put(Product.ProductContract.ProductEntry.ID_CATEGORY, product.getIdCategory());
        contentValues.put(Product.ProductContract.ProductEntry.INFO, product.getInfo());
        contentValues.put(Product.ProductContract.ProductEntry.AVAIL, product.getAvail());
        contentValues.put(Product.ProductContract.ProductEntry.PRICE, product.getPrice());
        db.update(Product.ProductContract.ProductEntry.TABLE_NAME,
                contentValues,
                Product.ProductContract.ProductEntry.ProductName +" = ? AND "+
                        Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND "+
                        Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?",
                new String[] {String.valueOf(product.getProductName()),
                        String.valueOf(product.getIdShop()),
                        String.valueOf(product.getIdCategory())});
    }

    public static Shop getShopById(int id) {
        for(Shop shop: MainActivity.mShops) {
            if(shop.getId() == id) {
                return shop;
            }
        }
        return null;
    }

    public static Category getCategoryById(int id) {
        for(Category category: MainActivity.mCategorys) {
            if(category.getId() == id) {
                return category;
            }
        }
        return null;
    }

    private ArrayList< JSONObject> getArrayListFromJSONArray(JSONArray jsonArray){
        ArrayList< JSONObject> aList = new ArrayList< JSONObject>();
        try {
            if(jsonArray!= null){
                for(int i = 0; i< jsonArray.length();i++){
                    aList.add(jsonArray.getJSONObject(i));
                    Log.d("getArrayListFromJSONArray", jsonArray.getJSONObject(i).toString());
                }
            } else Log.d("getArrayListFromJSONArray", "jsonArray == null");
        }catch (JSONException js){
            js.printStackTrace();
        }
        return aList;
    }

    public  static  String EncodingToUTF8(String response){
        try {
            byte[] code = response.toString().getBytes("ISO-8859-1");
            response = new String(code, "UTF-8");
        }catch (UnsupportedEncodingException e){
            return null;
        }
        return response;
    }

    public void getAllProducts(SQLiteDatabase db, int idShop, int idCategory,  RequestQueue requestQueue){
        mdb = db;
        midShop=idShop;
        midCategory=idCategory;
        mrequestQueue =requestQueue;
        if (midShop != 0) {
            if (MainActivity.listItems == null){
                final String url = "https://psv4.userapi.com/c534536/u140246498/docs/d42/f9469bb89151/products.json?extra=87kKdJeJ4DC17q1no39H2yTCdOsZ4_xKmo98PNCSxREIXiwsUCxyGYr3LL6OoXoSXDBekIAR4SJ10jbSKx0-J5vJeJAfP91lqDE7Ic71DCjWwTexoQ4Caznz45oaND8odTMp8aRLh_qqNmI9JpPu2lpw&dl=1";
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    ArrayList<JSONObject> listItems;
                                    JSONObject object = new JSONObject(EncodingToUTF8(response));
                                    JSONArray jsonArray = object.getJSONArray("products");
                                    listItems = getArrayListFromJSONArray(jsonArray);
                                    Log.d("Server operations", "done");
                                    String[] projections = {
                                            Product.ProductContract.ProductEntry.ProductName,
                                            Product.ProductContract.ProductEntry.ID_SHOP,
                                            Product.ProductContract.ProductEntry.ID_CATEGORY
                                    };
                                    GsonBuilder builder = new GsonBuilder();
                                    Gson gson = builder.create();

                                    //db request product name
                                    String selection = Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND " +
                                            Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?";
                                    String[] selectionArgs = new String[]{String.valueOf(midShop), String.valueOf(midCategory)};
                                    Cursor cursor = mdb.query(Product.ProductContract.ProductEntry.TABLE_NAME, projections,
                                            selection, selectionArgs, null, null, null);

                                    MainActivity.mProducts.clear();
                                    while (cursor.moveToNext()) {
                                        @SuppressLint("Range") String ProductName = cursor.getString(
                                                cursor.getColumnIndex(Product.ProductContract.ProductEntry.ProductName));
                                        Integer avail = new Integer(-1);
                                        Double price = new Double(-1);
                                        if (listItems != null) {
                                            for (JSONObject product : listItems) {
                                                Log.d("Server operations", product.toString());
                                                try {
                                                    if (product.getString(Product.ProductContract.ProductEntry.ID_SHOP).equals(String.valueOf(midShop))
                                                            && product.getString(Product.ProductContract.ProductEntry.ID_CATEGORY).equals(String.valueOf(midCategory))
                                                            && product.getString(Product.ProductContract.ProductEntry.ProductName).equals(String.valueOf(ProductName))) {
                                                        avail = Integer.valueOf(product.getString(Product.ProductContract.ProductEntry.AVAIL));
                                                        price = Double.valueOf(product.getString(Product.ProductContract.ProductEntry.PRICE));
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                        Product product = new Product(ProductName, getShopById(midShop), getCategoryById(midCategory), null, avail, price);
                                        MainActivity.mProducts.add(product);
                                    }
                                    MainActivity.listItems=listItems;
                                    MainActivity.mProductListAdapter.notifyDataSetChanged();

                                } catch (JSONException e) {
                                    Log.d("Server operations", e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Server operations", error.getMessage());
                            }
                        }
                );
                mrequestQueue.add(stringRequest);
            } else {
                String[] projections = {
                        Product.ProductContract.ProductEntry.ProductName,
                        Product.ProductContract.ProductEntry.ID_SHOP,
                        //Product.ProductContract.ProductEntry.INFO,
                        Product.ProductContract.ProductEntry.ID_CATEGORY
                };
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                //db request product name
                String selection = Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND " +
                        Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?";
                String[] selectionArgs = new String[]{String.valueOf(midShop), String.valueOf(midCategory)};
                Cursor cursor = mdb.query(Product.ProductContract.ProductEntry.TABLE_NAME, projections,
                        selection, selectionArgs, null, null, null);

                MainActivity.mProducts.clear();

                while (cursor.moveToNext()) {
                    @SuppressLint("Range") String ProductName = cursor.getString(
                            cursor.getColumnIndex(Product.ProductContract.ProductEntry.ProductName));
                    //@SuppressLint("Range") String info = cursor.getString(
                    //        cursor.getColumnIndex(Product.ProductContract.ProductEntry.INFO));
                    Integer avail = new Integer(-1);
                    Double price = new Double(-1);
                    if (MainActivity.listItems != null) {
                        for (JSONObject product : MainActivity.listItems) {
                            //Log.d("Server operations", product.toString());
                            try {
                                if (product.getString(Product.ProductContract.ProductEntry.ID_SHOP).equals(String.valueOf(midShop))
                                        && product.getString(Product.ProductContract.ProductEntry.ID_CATEGORY).equals(String.valueOf(midCategory))
                                        && product.getString(Product.ProductContract.ProductEntry.ProductName).equals(String.valueOf(ProductName))) {
                                    avail = Integer.valueOf(product.getString(Product.ProductContract.ProductEntry.AVAIL));
                                    price = Double.valueOf(product.getString(Product.ProductContract.ProductEntry.PRICE));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    Product product = new Product(ProductName, getShopById(midShop), getCategoryById(midCategory), null, avail, price);
                    MainActivity.mProducts.add(product);
                }
            }
        } else {
            String[] projections = {
                    Product.ProductContract.ProductEntry.ProductName,
                    Product.ProductContract.ProductEntry.ID_SHOP,
                    Product.ProductContract.ProductEntry.ID_CATEGORY,
                    //Product.ProductContract.ProductEntry.INFO,
                    Product.ProductContract.ProductEntry.AVAIL,
                    Product.ProductContract.ProductEntry.PRICE
            };
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            //db request product name, avail, price
            String selection = Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND " +
                    Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?";
            String[] selectionArgs = new String[]{String.valueOf(midShop), String.valueOf(midCategory)};
            Cursor cursor = mdb.query(Product.ProductContract.ProductEntry.TABLE_NAME, projections,
                    selection, selectionArgs, null, null, null);

            MainActivity.mProducts.clear();

            while (cursor.moveToNext()) {
                @SuppressLint("Range") String ProductName = cursor.getString(
                        cursor.getColumnIndex(Product.ProductContract.ProductEntry.ProductName));
                //@SuppressLint("Range") String info = cursor.getString(
                //        cursor.getColumnIndex(Product.ProductContract.ProductEntry.INFO));
                @SuppressLint("Range") Integer avail = cursor.getInt(
                        cursor.getColumnIndex(Product.ProductContract.ProductEntry.AVAIL));
                @SuppressLint("Range") Double price = cursor.getDouble(
                        cursor.getColumnIndex(Product.ProductContract.ProductEntry.PRICE));
                Product product = new Product(ProductName, getShopById(midShop), getCategoryById(midCategory), null, avail, price);
                MainActivity.mProducts.add(product);
            }
        }
    }

    public boolean existProduct(SQLiteDatabase db, Product product) {
        String[] projections = {
                Product.ProductContract.ProductEntry.ProductName
        };
        String selection =  Product.ProductContract.ProductEntry.ProductName +" = ? AND "+
                Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND "+
                Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?";
        String [] selectionArgs = new String[] { String.valueOf(product.getProductName()),
                String.valueOf(product.getIdShop()),
                String.valueOf(product.getIdCategory())};
        Cursor cursor = db.query(Product.ProductContract.ProductEntry.TABLE_NAME,
                projections,selection,selectionArgs,
                null,null,null );
        return cursor.getCount() > 0;
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void getInfo(SQLiteDatabase db, String productName, int shopId, int categoryId, EditText infoEdit) {
        String[] projections = {
                Product.ProductContract.ProductEntry.ProductName,
                Product.ProductContract.ProductEntry.ID_SHOP,
                Product.ProductContract.ProductEntry.ID_CATEGORY,
                Product.ProductContract.ProductEntry.INFO
        };
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        //db request product info
        String selection = Product.ProductContract.ProductEntry.ProductName + " = ? AND " + Product.ProductContract.ProductEntry.ID_SHOP + " = ? AND " +
                Product.ProductContract.ProductEntry.ID_CATEGORY + " = ?";
        String[] selectionArgs = new String[]{productName, String.valueOf(shopId), String.valueOf(categoryId)};
        Cursor cursor = db.query(Product.ProductContract.ProductEntry.TABLE_NAME, projections,
                selection, selectionArgs, null, null, null);
        cursor.moveToFirst();
        @SuppressLint("Range") String info = cursor.getString(
                   cursor.getColumnIndex(Product.ProductContract.ProductEntry.INFO));
            /*for (Product product : MainActivity.mProducts){
                if (product.getProductName().equals(productName) &&
                    product.getIdShop().equals(shopId) &&
                    product.getIdCategory().equals(categoryId))
                    product.setInfo(info);

            }*/
        infoEdit.setText(info);
    }
}
