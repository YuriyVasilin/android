package com.example.mylists.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.mylists.activity.MainActivity;
import com.example.mylists.model.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("ALL")
public class BT extends AsyncTask<String, Void, String> {
    Context context;
    public BT(Context context) {
        this.context = context;
    }
    @Override
    protected String doInBackground(String... strings) {
        DB dbOperations = new DB(context);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String method = strings[0];
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        if(method.equals("add_info")) {
            SQLiteDatabase db = dbOperations.getWritableDatabase();
            Product product = gson.fromJson(strings[1], Product.class);
            System.out.println("product from json: " + product);
            dbOperations.addInfo(db, product, requestQueue);
        }
        else if(method.equals("delete_product")) {
            SQLiteDatabase db = dbOperations.getWritableDatabase();
            Product product = gson.fromJson(strings[1], Product.class);
            System.out.println("product from json before delete: " + product);
            dbOperations.deleteProduct(db, product, requestQueue);
        }
        else if(method.equals("get_products")) {
            SQLiteDatabase db = dbOperations.getReadableDatabase();
            int shopId = Integer.parseInt(strings[1]);
            int categoryId = Integer.parseInt(strings[2]);
            dbOperations.getAllProducts(db, shopId, categoryId, requestQueue);
        }
        else if(method.equals("get_info")) {
            SQLiteDatabase db = dbOperations.getReadableDatabase();
            String productName = new String(strings[1]);
            int shopId = Integer.parseInt(strings[2]);
            int categoryId = Integer.parseInt(strings[3]);
            //dbOperations.getInfo(db, productName, shopId, categoryId);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        MainActivity.loadShopIntoNavigationView();
        MainActivity.mProductListAdapter.notifyDataSetChanged();
    }
}
