package com.example.mylists.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Shop implements Parcelable {
    private String name;
    private int id;
    private String phone;

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(id);
        parcel.writeString(phone);
    }

    public Shop(String name, int id, String phone) {
        this.name = name;
        this.id = id;
        this.phone = phone;
    }

    protected Shop(Parcel in) {
        name = in.readString();
        id = in.readInt();
        phone = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static final class ShopContract {
        public static abstract class ShopEntry {
            public static final String ID = "id";
            public static final String NAME = "name";
            public static final String PHONE = "phone";
            public static final String TABLE_NAME = "shop_table2";
        }
    }
}
