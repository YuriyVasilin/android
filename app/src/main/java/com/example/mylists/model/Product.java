package com.example.mylists.model;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.Comparator;
import java.util.Objects;

public class Product implements Parcelable {
    private String mProductName;
    private Integer IdShop;
    private Integer IdCategory;
    private String mInfo;
    private Integer avail;
    private Double price;

    public Product(String ProductName, Shop shop, Category category, String info, Integer Iavail, Double Iprice) {
        mProductName = ProductName;
        IdShop = shop.getId();
        IdCategory = category.getId();
        mInfo = info;
        avail = Iavail;
        price = Iprice;
    }

    public Product(String ProductName, Integer shopId, Integer categoryID, String info, Integer Iavail, Double Iprice) {
        mProductName = ProductName;
        IdShop = shopId;
        IdCategory = categoryID;
        mInfo = info;
        avail = Iavail;
        price = Iprice;
    }

    public Product(Product product) {
        mProductName = product.getProductName();
        IdShop = product.getIdShop();
        IdCategory = product.getIdCategory();
        mInfo = product.getInfo();
        avail = product.getAvail();
        price = product.getPrice();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return mProductName.equals(product.mProductName) && IdShop.equals(product.IdShop) && IdCategory.equals(product.IdCategory) && mInfo.equals(product.mInfo) && avail.equals(product.avail) && price.equals((product.price));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(mProductName, IdShop, IdCategory, mInfo, avail, price);
    }

    protected Product(Parcel in) {
        mProductName = in.readString();
        IdShop = in.readInt();
        IdCategory = in.readInt();
        mInfo = in.readString();
        avail = in.readInt();
        price = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mProductName);
        dest.writeInt(IdShop);
        dest.writeInt(IdCategory);
        dest.writeString(mInfo);
        dest.writeInt(avail);
        dest.writeDouble(price);
    }

    public static class CompareByPrice implements Comparator<Product>{
        @Override
        public int compare(Product t1, Product t2) {
            return t1.getPrice().compareTo(t2.getPrice());
        }
    }

    public static class CompareByAvail implements Comparator<Product>{
        @Override
        public int compare(Product t1, Product t2) {
            return t1.getAvail().compareTo(t2.getAvail());
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String ProductName) {
        mProductName = ProductName;
    }

    public Integer getIdShop() {
        return IdShop;
    }

    public void setIdShop(Integer IdShop) { this.IdShop = IdShop; }

    public Integer getIdCategory() { return IdCategory; }

    public void setIdCategory(Integer IdCategory) {
        this.IdCategory = IdCategory;
    }

    public Integer getAvail() { return avail; }

    public void setAvail(Integer avail) {
        this.avail = avail;
    }

    public Double getPrice() { return price; }

    public void setPrice(Double price) {
        this.price = price;
    }


    @Override
    public String toString() {
        return "Product{" +
                "mProductName='" + mProductName + '\'' +
                ", IdShop=" + IdShop +'\'' +
                ", IdCategory=" + IdCategory +'\'' +
                ", mInfo='" + mInfo + '\'' +
                ", avail='" + avail + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
    public Product() {
        mProductName = "";
        IdShop = -1;
        IdCategory = -1;
        mInfo = "";
        avail = -1;
        price = -1.0;
    }
    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String mInfo) {
        this.mInfo = mInfo;
    }

    public static final class ProductContract {
        public static abstract class ProductEntry {
            public static final String ID = "id";
            public static final String ProductName = "ProductName";
            public static final String ID_SHOP = "id_shop";
            public static final String ID_CATEGORY = "id_category";
            public static final String INFO = "info";
            public static final String AVAIL = "avail";
            public static final String PRICE = "price";
            public static final String TABLE_NAME = "product_table";
        }
    }
}
