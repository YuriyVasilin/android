package com.example.mylists.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.mylists.R;
import com.example.mylists.adapter.CategoryListAdapter;
import com.example.mylists.adapter.ProductListAdapter;
import com.example.mylists.adapter.ShopListAdapter;
import com.example.mylists.db.BT;
import com.example.mylists.model.Category;
import com.example.mylists.model.Product;
import com.example.mylists.model.Shop;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private int mPosition;
    private ActivityResultLauncher<Intent> mIntentActivityResultLauncher;
    private int checkedItemPosition;
    public static ArrayList<Product> mProducts;
    public static ProductListAdapter mProductListAdapter;
    public static ArrayList<JSONObject> listItems = null;
    ShopListAdapter shopListAdapter;
    CategoryListAdapter categoryListAdapter;
    SharedPreferences mPreferences;
    public static ArrayList<Shop> mShops;
    public static ArrayList<Category> mCategorys;
    public static String currentId = "-1 -1";
    public static int currentShopId = -1;
    public static int currentCategoryId = -1;
    public static Map<String, ArrayList<Product>> mapIdSCToProducts;
    private static DrawerLayout drawer;
    private static NavigationView navigationView;
    private static TabHost tabHost;
    private static Toolbar toolbar;
    public static boolean shopsCreated = false;

    /**
     * Disables the SSL certificate checking for new instances of {@link HttpsURLConnection} This has been created to
     * aid testing on a local box, not for use on production.
     */
    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void phone_calling() {
        String phone_number = mShops.get(currentShopId).getPhone();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone_number));
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CALL_PHONE
            }, 1);
        } else {
            try {
                startActivity(callIntent);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }
    public static void loadShopIntoNavigationView() {
        Menu drawerMenu = navigationView.getMenu();
        if (drawerMenu.size() != 0) return;
        for (Shop shop : mShops) {
            drawerMenu.add(shop.getName());
        }
    }

    public void loadProducts() {
        currentId = String.valueOf(currentShopId) + " " + String.valueOf(currentCategoryId);
        loadProductsFromDB();
        mProductListAdapter.notifyDataSetChanged();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void sortByAvail() {
        mProducts.sort(new Product.CompareByAvail());
        mProductListAdapter.notifyDataSetChanged();
        return;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void sortByPrice() {
        mProducts.sort(new Product.CompareByPrice());
        mProductListAdapter.notifyDataSetChanged();
        return;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.miAdd).setVisible(false);
        menu.findItem(R.id.miDelete).setVisible(false);
        menu.findItem(R.id.miEdit).setVisible(false);
        menu.findItem(R.id.miOrder).setVisible(false);
        menu.findItem(R.id.call).setVisible(false);
        menu.findItem(R.id.miExit).setVisible(true);
        if(currentShopId != -1) {
            menu.findItem(R.id.miEdit).setVisible(true);
            if (currentShopId != 0){
                menu.findItem(R.id.miOrder).setVisible(true);
                menu.findItem(R.id.call).setVisible(true);
            } else {
                menu.findItem(R.id.miDelete).setVisible(true);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Shop shop = getShopByName((String) item.getTitle());
        ActionBar toolbar = getSupportActionBar();
        if (shop.getId()!=0) toolbar.setTitle("Maгазин "+ shop.getName());
        else toolbar.setTitle("Мои "+ shop.getName());
        ArrayList<Product> oldProducts = new ArrayList<>();
        oldProducts.addAll(mProducts);
        mapIdSCToProducts.put(currentId, oldProducts);
        currentShopId = shop.getId();
        currentCategoryId = tabHost.getCurrentTab();
        currentId = String.valueOf(currentShopId) + String.valueOf(currentCategoryId);
        loadProducts();
        return false;
    }

    public void genetateData() {
        ArrayList <Product> products = new ArrayList<>();
        products.add(new Product("tovar1.1", 1, 0, "tovar1.1Info", null, null));
        products.add(new Product("tovar1.2", 1, 0, "tovar1.2Info", null, null));
        products.add(new Product("tovar1.3", 1, 0, "tovar1.3Info", null, null));
        products.add(new Product("tovar2.1", 1, 1, "tovar2.1Info", null, null));
        products.add(new Product("tovar2.2", 1, 1, "tovar2.2Info", null, null));
        products.add(new Product("tovar1.4", 2, 0, "tovar1.4Info", null, null));
        products.add(new Product("tovar1.5", 2, 0, "tovar1.5Info", null, null));
        products.add(new Product("tovar2.3", 2, 1, "tovar2.3Info", null, null));
        products.add(new Product("tovar2.4", 2, 1, "tovar2.4Info", null, null));
        products.add(new Product("tovar2.5", 2, 1, "tovar2.5Info", null, null));
        products.add(new Product("tovar1.6", 3, 0, "tovar1.6Info", null, null));
        products.add(new Product("tovar1.7", 3, 0, "tovar1.7Info", null, null));
        products.add(new Product("tovar2.6", 3, 1, "tovar2.6Info", null, null));
        products.add(new Product("tovar2.7", 3, 1, "tovar2.7Info", null, null));
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        for (Product product : products){
            BT backgroundTask = new BT(this);
            backgroundTask.execute("add_info", gson.toJson(product));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disableSSLCertificateChecking();
        setContentView(R.layout.main);
        checkedItemPosition = -1;
        if(mapIdSCToProducts == null) {
            mapIdSCToProducts = new HashMap<>();
        }

        createCategoryList();
        tabHost = (TabHost) findViewById(R.id.tabCat);
        tabHost.setup();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("meme1");
        tabSpec.setContent(R.id.llMiddle);
        tabSpec.setIndicator(mCategorys.get(0).getName());
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("meme1");
        tabSpec.setContent(R.id.llMiddle);
        tabSpec.setIndicator(mCategorys.get(1).getName());
        tabHost.addTab(tabSpec);
        TabHost.OnTabChangeListener l = new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                ArrayList<Product> oldProducts = new ArrayList<>();
                oldProducts.addAll(mProducts);
                mapIdSCToProducts.put(currentId, oldProducts);
                currentCategoryId = tabHost.getCurrentTab();
                currentId = String.valueOf(currentShopId) + String.valueOf(currentCategoryId);
                loadProducts();
            }
        };
        tabHost.setOnTabChangedListener(l);
        tabHost.setCurrentTab(0);
        currentCategoryId = 0;
        createShopList();
        ActionBar toolbar = getSupportActionBar();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.app_name, R.string.app_name);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        loadShopIntoNavigationView();
        navigationView.setNavigationItemSelectedListener( this);
        // disply home button for actionbar
        toolbar.setDisplayHomeAsUpEnabled(true);

        if(mProducts == null) createProductList();
        loadProducts();

        mIntentActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if(result.getResultCode() == Activity.RESULT_OK) {
                            Intent intent = result.getData();
                            Product p = intent.getParcelableExtra("product");
                            if(mPosition  == mProducts.size() + 1) {
                                if(p.getIdShop() != currentShopId || p.getIdCategory() != currentCategoryId) {
                                    //добавляем в другой раздел, не переходим
                                    String newPlace = p.getIdShop()+" "+p.getIdCategory();
                                    ArrayList<Product> oldProduct = mapIdSCToProducts.get(newPlace);
                                    if(oldProduct == null) {
                                        oldProduct = new ArrayList<Product>();
                                    }
                                    oldProduct.add(p);
                                    mapIdSCToProducts.put(newPlace, oldProduct);
                                }
                                //добавляем в текущий раздел
                                else mProducts.add(p);
                            }
                            else {
                                if(p.getIdShop() != currentShopId || p.getIdCategory() != currentCategoryId) {
                                    //переносим в другой раздел
                                    String newPlace = p.getIdShop()+" "+p.getIdCategory();
                                    ArrayList<Product> oldProduct = mapIdSCToProducts.get(newPlace);
                                    if(oldProduct == null) {
                                        oldProduct = new ArrayList<Product>();
                                    }
                                    oldProduct.add(p);
                                    mapIdSCToProducts.put(newPlace, oldProduct);
                                    mProducts.remove(mPosition);
                                }
                                //меняем в нашем разделе
                                else mProducts.set(mPosition, p);
                            }
                            mProductListAdapter.notifyDataSetChanged();
                            Toast.makeText(getApplicationContext(),
                                    "Товар: " + p.getProductName().toString() + "\nУспешно сохранён", Toast.LENGTH_SHORT).show();
                        }else if (result.getResultCode() == Activity.RESULT_CANCELED){

                        }
                    }
                }
        );
        genetateData();
        tabHost.setCurrentTab(1);
        tabHost.setCurrentTab(0);


    }
    public static Shop getShopById(int id) {
        for(Shop shop: mShops) {
            if(shop.getId() == id) {
                return shop;
            }
        }
        return null;
    }

    public static Category getCategoryById(int id) {
        for(Category category: mCategorys) {
            if(category.getId() == id) {
                return category;
            }
        }
        return null;
    }

    public Shop getShopByName(String name) {
        for(Shop shop : mShops) {
            if(shop.getName().equals(name)) {
                return shop;
            }
        }
        return null;
    }

    public Category getCategoryByName(String name) {
        for(Category category : mCategorys) {
            if(category.getName().equals(name)) {
                return category;
            }
        }
        return null;
    }

    public void createShopList() {
        if (!shopsCreated){
            if(mShops == null) mShops = new ArrayList<>();
            mShops.add(new Shop("Заказы", 0, "0"));
            mShops.add(new Shop("CityLink", 1, "89181234567"));
            mShops.add(new Shop("DNS", 2, "89181234568"));
            mShops.add(new Shop("Mvideo", 3, "89181234569"));
            shopsCreated = true;
        }
    }

    public void createCategoryList() {
        if(mCategorys == null) mCategorys = new ArrayList<>();
        mCategorys.add(new Category("Smartphones", 0));
        mCategorys.add(new Category("Notebooks", 1));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        ListView listView = findViewById(R.id.lvList2);
        int position = listView.getCheckedItemPosition();

        switch (item.getItemId()){
            case android.R.id.home:
                if(drawer.isOpen()) {
                    drawer.close();
                }
                else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.miEdit:{
                if(listView.isSelected()){
                    boolean order = false;
                    Intent intent = new Intent(MainActivity.this, ProductInfoActivity.class);
                    intent.putExtra("product", mProducts.get(position));
                    intent.putParcelableArrayListExtra("shops", mShops);
                    intent.putParcelableArrayListExtra("categorys", mCategorys);
                    intent.putExtra("currentShop", currentShopId);
                    intent.putExtra("currentCategory", currentCategoryId);
                    intent.putExtra("order", order);
                    mPosition=position;
                    mIntentActivityResultLauncher.launch(intent);
                }else {
                    Toast.makeText(getApplicationContext(),
                            "Товар не выбран", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            case R.id.miAdd:{
                boolean order = false;
                Intent intent = new Intent(MainActivity.this, ProductInfoActivity.class);
                Product p = new Product();
                p.setIdShop(currentShopId);
                p.setIdCategory(currentCategoryId);
                intent.putExtra("product", p);
                intent.putExtra("currentShop", currentShopId);
                intent.putExtra("currentCategory", currentCategoryId);
                intent.putParcelableArrayListExtra("shops", mShops);
                intent.putParcelableArrayListExtra("categorys", mCategorys);
                intent.putExtra("order", order);
                mPosition=mProducts.size() + 1;
                mIntentActivityResultLauncher.launch(intent);
                return true;
            }
            case R.id.miDelete:{
                if(listView.isSelected()) {
                    AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                            MainActivity.this);
                    quitDialog.setTitle("Удалить заказ? \"" + mProducts.get(position).getProductName() + "\"?");

                    quitDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // delete product
                            deleteData(mProducts.get(position));
                            mProducts.remove(position);
                            mProductListAdapter.notifyDataSetChanged();
                        }
                    })
                            .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    quitDialog.show();
                }else {
                    Toast.makeText(getApplicationContext(),
                            "Товар не выбран", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            case R.id.miOrder:{

                if(listView.isSelected()){
                    boolean order = true;
                    Intent intent = new Intent(MainActivity.this, ProductInfoActivity.class);
                    Product p = mProducts.get(position);
                    intent.putExtra("product", mProducts.get(position));
                    intent.putParcelableArrayListExtra("shops", mShops);
                    intent.putParcelableArrayListExtra("categorys", mCategorys);
                    intent.putExtra("currentShop", currentShopId);
                    intent.putExtra("currentCategory", currentCategoryId);
                    intent.putExtra("order", order);
                    mPosition=position;
                    mIntentActivityResultLauncher.launch(intent);
                }else {
                    Toast.makeText(getApplicationContext(),
                            "Товар не выбран", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            case R.id.miExit:{
                finish();
                return true;
            }
            case R.id.call:{
                phone_calling();
                return true;
            }
            default:{}
        }
        return super.onOptionsItemSelected(item);
    }

    public void createProductList() {
        mProducts=new ArrayList<>();
        ListView listView = findViewById(R.id.lvList2);
        mProductListAdapter=new ProductListAdapter(mProducts,this);
        listView.setAdapter(mProductListAdapter);
        AdapterView.OnItemClickListener clProduct = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(checkedItemPosition != position){
                    listView.setItemChecked(position,true);
                    listView.setSelected(true);
                    checkedItemPosition = position;
                }else {
                    listView.setItemChecked(position,false);
                    listView.setSelected(false);
                    checkedItemPosition = -1;
                }
                mProductListAdapter.colorChecked(position,parent);
            }
        };
        listView.setOnItemClickListener(clProduct);
    }
    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop");
        /*if (mProducts != null && currentCategoryId != 0){
            for(Product product: mProducts) {
                System.out.println("saveData");
                saveData(product);
            }
        }*/

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    public void saveData(Product product) {
        BT backgroundTask = new BT(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("add_info", gson.toJson(product));

    }
    public void loadProductsFromDB() {
        if(currentShopId != -1 && currentCategoryId != -1) {
            mProducts.clear();
            if(mapIdSCToProducts.containsKey(currentId) && currentShopId!=0) {
                ArrayList<Product> oldProducts = mapIdSCToProducts.get(currentId);
                if(!oldProducts.isEmpty()) {
                    mProducts.addAll(oldProducts);
                    mProductListAdapter.notifyDataSetChanged();
                }

            }
            else {
                BT backgroundTask = new BT(this);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                backgroundTask.execute("get_products", String.valueOf(currentShopId), String.valueOf(currentCategoryId));
            }
        }
    }
    public void deleteData(Product product) {
        BT backgroundTask = new BT(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("delete_product", gson.toJson(product));
    }
}