package com.example.mylists.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mylists.R;
import com.example.mylists.adapter.CategoryListAdapter;
import com.example.mylists.adapter.ShopListAdapter;
import com.example.mylists.db.BT;
import com.example.mylists.db.DB;
import com.example.mylists.model.Category;
import com.example.mylists.model.Product;
import com.example.mylists.model.Shop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class ProductInfoActivity extends AppCompatActivity {

    private Product p;
    private ArrayList<Shop> shops;
    private ArrayList<Category> categorys;
    private int checkedItemPosition;
    private Spinner myShopSpinner;
    private Spinner myCategorySpinner;
    private ShopListAdapter shopListAdapter;
    private CategoryListAdapter categoryListAdapter;
    private int currentShop;
    private int currentCategory;
    private boolean order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);
        checkedItemPosition = -1;
        order = getIntent().getBooleanExtra("order", false);
        currentShop = getIntent().getIntExtra("currentShop", -1);
        currentCategory = getIntent().getIntExtra("currentCategory", -1);
        p = getIntent().getParcelableExtra("product");
        shops = getIntent().getParcelableArrayListExtra("shops");
        categorys = getIntent().getParcelableArrayListExtra("categorys");

        myShopSpinner = (Spinner) findViewById(R.id.editShop);
        shopListAdapter = new ShopListAdapter(this, android.R.layout.simple_spinner_item, shops);
        myShopSpinner.setAdapter(shopListAdapter);
        AdapterView.OnItemSelectedListener sl = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Shop currentShop = shopListAdapter.getItem(position);
                p.setIdShop(currentShop.getId());
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        };
        myShopSpinner.setOnItemSelectedListener(sl);
        myShopSpinner.setSelection(p.getIdShop());

        myCategorySpinner = (Spinner) findViewById(R.id.editCategory);
        categoryListAdapter = new CategoryListAdapter(this, android.R.layout.simple_spinner_item, categorys);
        myCategorySpinner.setAdapter(categoryListAdapter);
        AdapterView.OnItemSelectedListener cl = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Category currentCategory = categoryListAdapter.getItem(position);
                p.setIdCategory(currentCategory.getId());
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        };
        myCategorySpinner.setOnItemSelectedListener(cl);
        myCategorySpinner.setSelection(p.getIdCategory());

        ((EditText) findViewById(R.id.editProductName)).setText(p.getProductName());
        ((EditText) findViewById(R.id.editPrice)).setText(String.valueOf(p.getPrice()));
        ((EditText) findViewById(R.id.editAvail)).setText(String.valueOf(p.getAvail()));
        if (order){
            findViewById(R.id.orderLayout).setVisibility( View.VISIBLE );
            ((EditText) findViewById(R.id.editOrder)).setText("0");
        }else{
            findViewById(R.id.orderLayout).setVisibility( View.INVISIBLE );
            ((EditText) findViewById(R.id.editOrder)).setText("-1");
        }

        //EditText infoEdit = findViewById(R.id.editInfo);
        //DB dbOperations = new DB(this);
        //SQLiteDatabase db = dbOperations.getReadableDatabase();
        //dbOperations.getInfo(db, p.getProductName(), p.getIdShop(), p.getIdCategory(), infoEdit);
        //infoEdit.setKeyListener(null);

        ((EditText) findViewById(R.id.editProductName)).setKeyListener(null);
        ((EditText) findViewById(R.id.editPrice)).setKeyListener(null);
        ((EditText) findViewById(R.id.editAvail)).setKeyListener(null);
        myShopSpinner.setEnabled(false);
        myCategorySpinner.setEnabled(false);

        class DBRequest extends AsyncTask<Context, Context, Context> {
            @Override
            protected Context doInBackground(Context... contexts) {
                EditText infoEdit = findViewById(R.id.editInfo);
                DB dbOperations = new DB(contexts[0]);
                SQLiteDatabase db = dbOperations.getReadableDatabase();
                dbOperations.getInfo(db, p.getProductName(), p.getIdShop(), p.getIdCategory(), infoEdit);
                infoEdit.setKeyListener(null);
                return contexts[0];
            }

            @Override
            protected void onPostExecute(Context context) {
                return;
            }
        }
        DBRequest request = new DBRequest();
        request.execute(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_menu, menu);
        return true;
    }


    public void productSave(View view) {
        p.setProductName(((EditText) findViewById(R.id.editProductName)).getText().toString());
        p.setInfo(((EditText) findViewById(R.id.editInfo)).getText().toString());
        p.setPrice(Double.valueOf(((EditText) findViewById(R.id.editPrice)).getText().toString()));
        Shop shop = shopListAdapter.getItem(myShopSpinner.getSelectedItemPosition());
        p.setIdShop(shop.getId());
        Category category = categoryListAdapter.getItem(myCategorySpinner.getSelectedItemPosition());
        p.setIdCategory(category.getId());
        if (order) {
            Product pO = new Product(p);
            pO.setIdShop(0);
            pO.setAvail(Integer.valueOf(((EditText) findViewById(R.id.editOrder)).getText().toString()));
            saveData(pO);
            //p.setAvail(Integer.valueOf(((EditText) findViewById(R.id.editAvail)).getText().toString())
            //        -Integer.valueOf(((EditText) findViewById(R.id.editOrder)).getText().toString()));
        } else p.setAvail(Integer.valueOf(((EditText) findViewById(R.id.editAvail)).getText().toString()));
        saveData(p);
        Intent intent = new Intent();
        intent.putExtra("product", p);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void clExit(View view) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
    public int getIdShop(String name) {
        for(Shop shop: shops) {
            if(shop.getName().equals(name)) {
                return shop.getId();
            }
        }
        return -1;
    }
    public String getNameShop() {
        String nameShop = "";
        for(Shop shop: shops) {
            if(shop.getId() == currentShop) {
                nameShop = shop.getName();
            }
        }
        return nameShop;
    }

    public int getIdCategory(String name) {
        for(Category category: categorys) {
            if(category.getName().equals(name)) {
                return category.getId();
            }
        }
        return -1;
    }
    public String getNameCategory() {
        String nameCategory = "";
        for(Category category: categorys) {
            if(category.getId() == currentCategory) {
                nameCategory = category.getName();
            }
        }
        return nameCategory;
    }


    @Override
    public void onBackPressed() {
        boolean err = false;
        if(TextUtils.isEmpty(((EditText) findViewById(R.id.editProductName)).getText().toString())){
            ((EditText) findViewById(R.id.editProductName)).setError("Не указано Наименование товара");
            err = true;
        }
        if(!myShopSpinner.isSelected()){
            p.setIdShop(currentShop);
        }
        if(!myCategorySpinner.isSelected()){
            p.setIdCategory(currentCategory);
        }
        if(TextUtils.isEmpty(((EditText) findViewById(R.id.editPrice)).getText().toString())){
            ((EditText) findViewById(R.id.editPrice)).setError("Не указана Цена");
            err = true;
        }
        if(TextUtils.isEmpty(((EditText) findViewById(R.id.editAvail)).getText().toString())){
            ((EditText) findViewById(R.id.editAvail)).setError("Не указано Количество");
            err = true;
        }
        if(TextUtils.isEmpty(((EditText) findViewById(R.id.editInfo)).getText().toString())){
            ((EditText) findViewById(R.id.editInfo)).setError("Не указана Информация");
            err = true;
        }
        if (order){
            if((Integer.valueOf(((EditText) findViewById(R.id.editOrder)).getText().toString())) <= 0){
                ((EditText) findViewById(R.id.editOrder)).setError("Указано неверное количество для заказа");
                err = true;
            } else {
                if (Integer.valueOf(((EditText) findViewById(R.id.editOrder)).getText().toString())>Integer.valueOf(((EditText) findViewById(R.id.editAvail)).getText().toString())){
                    ((EditText) findViewById(R.id.editOrder)).setError("Количество заказа превышает доступное количество");
                    err = true;}
            }
        }

        AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                this);
        if(!err) {
            if (order) {
                quitDialog.setTitle("Сохранить заказ?");
            } else {
                quitDialog.setTitle("Выйти из окна детализации?");
            }
            quitDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (order) productSave(null);
                    clExit(null);
                }
            })
                    .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (order) clExit(null);
                        }
                    })
            ;

        }
        else {
            quitDialog = new AlertDialog.Builder(
                    this);
            quitDialog.setTitle("Ошибка в заполнении полей. Закрыть окно?")
                    .setPositiveButton("Закрыть", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            clExit(null);
                        }
                    })
                    .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    })
            ;
        }
        quitDialog.show();

    }
    public void saveData(Product product) {
        System.out.println("PIA saveData product: " + product);
        BT backgroundTask = new BT(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("add_info", gson.toJson(product));
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
}